import neo4j, {Session} from 'neo4j-driver'

// HOW TO USE PARAMETERS IN QUERIES: https://neo4j.com/docs/cypher-manual/current/syntax/parameters/
// https://datatables.net/manual/server-side

/*

*/
export class CacheServer_DBActions {
    session: () => Session;

    constructor(neo4jSession: () => Session) {
        this.session = neo4jSession;
    }

    async putInCache(category:string, task_type:string, key: string, value: string) {
        if (!category || category == "")    throw new Error("category is required to put key in cache!");
        if (!key || key == "")              throw new Error("key is required to put key in cache!");
        if (!value)                         throw new Error("value is required to put key in cache!");

        await this.session().writeTransaction(async tx => {
            await tx.run(`
                MERGE (c:CacheEntry {
                        category:  $category,
                        task_type: $task_type,
                        key:       $key,
                        value:     $value  
                    }
                )
            `, {
                    category:   category,
                    task_type:  task_type,
                    key:        key,
                    value:      value
            })
        });
    }

    async getFromCache(category:string, task_type:string, key: string) {
        if (!category || category == "")    throw new Error("category is required to get from cache!");
        if (!key || key == "")              throw new Error("key is required to get from cache");

        return await this.session().writeTransaction(async tx => {
            let result = await tx.run(`
                 MATCH (c:CacheEntry {
                        category:  $category,
                        task_type: $task_type,
                        key:       $key
                    }
                )
                RETURN c.value
            `, {
                category:   category,
                task_type:  task_type,
                key:        key
            })
            if (result.records.length > 0) {
                return result.records[0].get(0)
            }
            else                           { return null;                                        }
        });
    }
}