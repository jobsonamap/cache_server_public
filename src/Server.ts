import express, {RequestHandler} from 'express';
import bodyParser from 'body-parser';
import path from "path";
import fs from "fs";
import { fileURLToPath } from "url";
import passport from "passport";

import passport_local from 'passport-local';
// const { Strategy as LocalStrategy } = pkg;

import connect_ensure_login from 'connect-ensure-login';


// @ts-ignore
import MongoDataTable from 'mongo-datatable';
import * as core from "express-serve-static-core";
import neo4j, {Driver, Session} from "neo4j-driver";
import {loadSettings} from "./Settings.js";
import {constants} from "http2";
import {CacheServer_DBActions} from "./dbactions/CacheServer_DBActions.js";
import cookieParser from 'cookie-parser';
import express_session from 'express-session';
import axios from "axios";

/*
    TESTING: this api is tested with postman.
    When you are not given a link, ask for the postman link.
    It contains a collection of example API calls that allow to test this component.
 */
export class Server {

  settings:any;

  constructor(settings:any) {
    this.settings = settings;
  }

    async post(url:string, data:object) {
        try {
            console.log("posting to " + url + " data: " + JSON.stringify(data));
            let result = await axios.post(url, data);
            console.log("posting done, got result: " + JSON.stringify(result.data));
            return result;
        } catch (error) {
            let indented_stacktrace = (<string>error.response.data).split("\n").map(l => "  " + l).join("\n");
            let log_description =
                `When calling ${url} with data: ${JSON.stringify(data)} got response:\n` +
                `${error.response.status} ${error.response.statusText} with stacktrace:\n${indented_stacktrace}`;
            console.error(log_description);
            throw new Error(log_description);
        }
    }

    async get(url:string, data:object) {
        try {
            console.log("getting from " + url + " data: " + JSON.stringify(data));
            let result = await axios.get(url, data);
            console.log("getting done, got result: " + JSON.stringify(result.data));
            return result;
        } catch (error) {
            let indented_stacktrace = (<string>error.response.data).split("\n").map(l => "  " + l).join("\n");
            let log_description =
                `When calling ${url} with data: ${JSON.stringify(data)} got response:\n` +
                `${error.response.status} ${error.response.statusText} with stacktrace:\n${indented_stacktrace}`;
            console.error(log_description);
            throw new Error(log_description);
        }
    }

  async add_page_tasks(app: core.Express, loginGuard : RequestHandler, workmanagerActions: CacheServer_DBActions) {

      app.post("/put_in_cache.json",async (req, res) => {
          let body      = req.body;
          let category  = (<any>body).category;
          let task_type = (<any>body).task_type;
          let key       = (<any>body).key;
          let value     = (<any>body).value;

          await workmanagerActions.putInCache(category, task_type, key, value);
          res.sendStatus(200);
      });

      app.post("/get_from_cache.json",async (req, res) => {
          let body      = req.body;
          let category  = (<any>body).category;
          let task_type = (<any>body).task_type;
          let key       = (<any>body).key;
          let value     = await workmanagerActions.getFromCache(category, task_type, key);
          res.json({
              value: value
          });
      });
  }

  async addPages(app: core.Express, loginGuard : RequestHandler, driver : Driver, database: string) {
    const tasksActions  = new CacheServer_DBActions(() => driver.session({ database : database}));
    await this.add_page_tasks(app, loginGuard, tasksActions);
  }
}