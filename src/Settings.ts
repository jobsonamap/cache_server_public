import path from "path";
import {fileURLToPath} from "url";
import fs from "fs";

export async function loadSettings() {
    // note: override default settings with local one
    // by passing the env variable ENVIRONMENT_SETTING_FILE

    const __dirname = path.dirname(fileURLToPath((<any>import.meta).url));
    let env_file = path.join(__dirname, "..", "environment_variables.json");
    env_file = process.env.ENVIRONMENT_SETTING_FILE? process.env.ENVIRONMENT_SETTING_FILE: env_file;

    // be user friendly
    console.log("Loading environment variables file...  " + env_file);
    console.log("To change, start the proccess with the system variable ENVIRONMENT_SETTING_FILE set to the environment variables file, e.g:");
    console.log("my_linux_promt>>ENVIRONMENT_SETTING_FILE=/apps/settings/myserverappsettings.json; node Index.js");

    // load the settings
    let settingsStr  =fs.readFileSync(env_file, {encoding: "utf8"});

    // inject environment variables
    console.log("in config file:")
    for (let key of Object.keys(process.env)) {
        // replace backslaches in paths, since these cause the json parsing to fail
        let value = process.env[key].replace(/\\/g, "/");
        console.log(`   substituting $${key.padEnd(30, " ")} with ${value.padEnd(30)}`);
        settingsStr = settingsStr.replace("$" + key, value);
    }

    console.log("\nusing settings:");
    console.log(settingsStr);

    let settingsObject =JSON.parse(settingsStr);
    return settingsObject;
}