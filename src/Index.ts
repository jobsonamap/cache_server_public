import "source-map-support/register.js";
import path from "path";
import {fileURLToPath} from "url";
import {Server} from "./Server.js";
import {NextFunction, RequestHandler} from "express";
import bodyParser from 'body-parser';
import passport from "passport";
import passport_local from 'passport-local';
import connect_ensure_login from 'connect-ensure-login';
import * as core from "express-serve-static-core";
import neo4j, {Driver, Session} from "neo4j-driver";
import {loadSettings} from "./Settings.js";
import express from 'express';
import express_session from 'express-session';

function setupLogin(app: core.Express, users: { username:string, password:string }[]) {
	// setup passport
	passport.use(new passport_local.Strategy(
		function(username, password, done) {
			let user = users.filter(u => u.username == username && u.password == password)[0];
			if (user != undefined) { return done(null, user); }
			else                   { return done(null, null); }
		}
	));
	passport.serializeUser(function(user, done) { done(null, user); });
	passport.deserializeUser(function(user, done) { done(null, user); });
	app.use(passport.initialize());
	app.use(passport.session());
	app.get('/login', function(req, res){ res.render('login'); });
	app.post('/login', passport.authenticate('local', { failureRedirect: '/login', successRedirect: '/' }));
	let loginGuard : RequestHandler =  connect_ensure_login.ensureLoggedIn('/login');
	return loginGuard;
}

function createRequestLogger() : express.RequestHandler {
	// @ts-ignore
	return (req:Request, res:Response, next:NextFunction) => {
		let current_datetime = new Date();
		let formatted_date = current_datetime.toUTCString();
		let method 	= req.method;
		let url 	= req.url;
		let status 	= res.status;
		let body = req.body? JSON.stringify(req.body): "";
		if (body != "" && body != "{}") console.log(`processing request: ${formatted_date} ${method} ${url} \nwith body         : ${body}`);
		else            				console.log(`processing request: ${formatted_date} ${method} ${url}`);
		let jsonFN = res.json;
		jsonFN = jsonFN.bind(res);

		// @ts-ignore
		res.json = async (obj:object) => {
			console.log(`sending back      : ${JSON.stringify(obj)}`);
			// @ts-ignore
			return jsonFN(obj);
		};
		next();
	};
}

async function main() {
	let settings = await loadSettings();
	let users = settings.users;

	// configure express
	const __dirname = path.dirname(fileURLToPath((<any>import.meta).url));
	const app = express();
	app.set("views", path.join(__dirname, "views"));
	app.set("view engine", "pug");
	app.use(express.static( path.join(__dirname, 'resources')));
	app.use(bodyParser.raw());
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json({ limit: "1TB" }));
	app.use(express_session({ secret: 'keyboard cat' }));
	app.use(createRequestLogger());
	let loginGuard : RequestHandler = setupLogin(app, users);

	// connect to neo4j
	let neo4j_db_settings = settings.neo4j_db_settings;
	let url = neo4j_db_settings.url;
	let user = neo4j_db_settings.user;
	let pass = neo4j_db_settings.pass;
	let database = neo4j_db_settings.database;
	console.log("connecting to url: " + url);
	console.log("with user / pass: " + user + "/" + pass + " " + database);
	const driver: Driver = neo4j.driver(url, neo4j.auth.basic(user, pass), {
		connectionTimeout: 5 * 1000,
		connectionAcquisitionTimeout: 2 * 1000 // 2 seconds
	})
	let serverInfo = await driver.verifyConnectivity();
	console.log("verified connection to neo4j server on " + serverInfo.address + " of version " + serverInfo.version);

	// add paths and start server
	let urlServer = new Server(settings);
	await urlServer.addPages(app, loginGuard, driver, database);

	// add error handling functionality (return a plain text stacktrace)
    // can be accessed like this: { axios.get("http://ip/call_some_rest_method") }
    // catch (error) { console.log(error.response.statusText + ":\n" + error.response.data); }
    app.use((err: any, req: any, res: any, next: any) => { res.status(500).send(err.stack); });
	app.listen(settings.port, () => console.log(`Server listening at http://localhost:${settings.port}`));
}

main();
