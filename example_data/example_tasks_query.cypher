MATCH (a:Address)<-[:HAS_ADDRESS]-(o:Object)-[rel:HAS_TASK]->(t:Task)-[:HAS_EXECUTION]->(e:Execution)
  WHERE
  // filter by address
  a.country     = $country 				                  AND
  a.city        = $city 				                    AND
  a.street      = $street 				                  AND
  a.houseNumber = $houseNumber 			                AND

  // filter by generic task fields
  t.status               = $status 			            AND
  t.assigned_to          = $assigned_to 		        AND
  t.created_on           < $min_created_on 		      AND
  t.created_on           > $max_created_on 		      AND
  t.batch                = $batch                   AND
  t.reschedule           = $reschedule              AND
  t.priority             = $priority                AND

  // if this task is the last created task of tis type, for the object, its relation will be current
  // this property will be maintained by triggers (see below)
  // we may want only see the current tasks,
  // or all historic tasks  between a range of time
  rel.isCurrent          = $current                 AND

  // Task type and implied object filter section
  t:DomainFind                                      AND
  t:JobListingSniff				                          AND
  t:NameClassification		                          AND
  t:NumberOfEmployees				                        AND
  o:Entity                                      		AND
  o.hasUrl			= $hasUrl 			                    AND
  o.hasListingUrl	        = $hasListingUrl          AND

  t:Tag				                                      AND
  t:Crawl							                              AND
  t:Diff						                                AND
  o:ListingUrl                                      AND
  o.navigationStrategy	= $navigationStrategy 		  AND
  o.detailStrategy		  = $detailStrategy 		      AND
  o.hasBug		 	        = $hasBug

WITH DISTINCT t
// when a task passes the filter, we still want to collect all of its information
MATCH (a:Address)<-[:HAS_ADDRESS]-(o:Object)-[:HAS_TASK]->(t:Task)-[:HAS_EXECUTION]->(e:Execution)
  WHERE
  e.started_on   		      	>= $exc_ran_between_min           AND
  e.finished_on 		      	<= $exc_ran_between_max           AND
  e.finished_on - e.started_one 	>  $exc_ran_longer_then     AND
  e.executed_by                 	=  $exc_executed_by         AND
  e.finished_as       	      	=  $exc_finished_as           // "SUCCESSFULL", "IN_ERROR"
RETURN {
    id:t.id,
    type:labels(t)[0],                                            // probabily incorrect but ok for now
    executions: collect(distinct {resultSummary: e.resultSummary}),
    addresses:  collect(distinct {country: a.country, city: a.city, street: a.street, houseNumber:a.houseNumber}),
    objects:    collect(distinct {type: labels(o)[0], id:o.id })
}
